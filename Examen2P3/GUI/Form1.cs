﻿using Examen2P3.BO;
using Examen2P3.ENTIDADES;
using Examen2P3.GUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Examen2P3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            cargarColaborador();
        }
        public void cargarColaborador()
        {
            
            foreach(Colaborador c in new ColaboradorBO().cargarColaboradores())
            {
                cbxColaborador.Items.Add(c.nombre);
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmAgregarTarea frm = new FrmAgregarTarea();
            this.Hide();
            frm.Show();
           
        }
    }
}
