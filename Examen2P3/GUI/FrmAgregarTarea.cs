﻿using Examen2P3.BO;
using Examen2P3.ENTIDADES;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Examen2P3.GUI
{
    public partial class FrmAgregarTarea : Form
    {
        public FrmAgregarTarea()
        {
            InitializeComponent();
            cargarColaborador();
            cbxEstado.SelectedItem = "Pendiente";
            cbxEstado.Enabled = false;
        }
        public void cargarColaborador()
        {

            foreach (Colaborador c in new ColaboradorBO().cargarColaboradores())
            {
                cbxColaborador.Items.Add(c.nombre);
            }
        }
        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void FrmAgregarTarea_Load(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
