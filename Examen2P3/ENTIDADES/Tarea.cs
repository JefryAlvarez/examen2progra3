﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen2P3.ENTIDADES
{
    class Tarea
    {
        public int Id { get; set; }
        public String Descripcion { get; set; }
        public int Id_Colaborador { get; set; }
        public String Estado { get; set; }
        public String Prioridad { get; set; }
        public DateTime Fecha_inicial { get; set; }
        public DateTime Fecha_final { get; set; }
        public String notas { get; set; }

    }
}
