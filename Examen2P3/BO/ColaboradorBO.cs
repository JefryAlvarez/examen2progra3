﻿using Examen2P3.DAL;
using Examen2P3.ENTIDADES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen2P3.BO
{
    class ColaboradorBO
    {
        public List<Colaborador> cargarColaboradores()
        {
            return new ColaboradorDAL().CargarDatos();
        }
    }
}
