﻿using Examen2P3.ENTIDADES;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen2P3.DAL
{
    class ColaboradorDAL
    {
        public NpgsqlCommand cmd;
        public NpgsqlConnection con;
        Conexion conexion = new Conexion();
        public List<Colaborador> CargarDatos()
        {
            con = conexion.ConexionBD();

            List<Colaborador> h = new List<Colaborador>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT * FROM public.colaborador", con);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    h.Add(cargarHorario(dr));
                }
            }
            con.Close();
            return h;
        }

        public Colaborador cargarHorario(NpgsqlDataReader dr)
        {
            Colaborador h = new Colaborador();
            h.Id = dr.GetInt32(0);
            h.nombre = dr.GetString(1);
            return h;
        }
    }
}
