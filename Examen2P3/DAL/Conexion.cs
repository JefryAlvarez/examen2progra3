﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Examen2P3.DAL
{
    class Conexion
    {
        XmlDocument doc;
        public NpgsqlConnection conexion;
        public NpgsqlCommand cmd;
        string servidor;
        int puerto;
        string usuario;
        string clave;
        string baseDatos;

        public DataSet CargarIni()
        {

            DataSet dsetConf = new DataSet();
            try
            {
                string ArchivoXML = System.Environment.CurrentDirectory + "\\INI.xml";
                using (System.IO.FileStream fsReadXml = new System.IO.FileStream(ArchivoXML, System.IO.FileMode.Open))
                {
                    dsetConf.ReadXml(fsReadXml);
                }
                DataRow fila = dsetConf.Tables[0].Rows[0];
                servidor = fila["Server"].ToString();
                puerto = Int32.Parse(fila["Port"].ToString());
                usuario = fila["Usuario"].ToString();
                clave = fila["Password"].ToString();
                baseDatos = fila["Database"].ToString();

            }
            catch (Exception e)
            {
                throw e;
            }
            return dsetConf;
        }
        public NpgsqlConnection ConexionBD()
        {
            CargarIni();


            string cadenaConexion = "Server=" + servidor + ";" + "Port=" + puerto + ";" + "User Id=" + usuario + ";" + "Password=" + clave + ";" + "Database=" + baseDatos;
            conexion = new NpgsqlConnection(cadenaConexion);
            conexion.Open();

            return conexion;

        }
    }
}
